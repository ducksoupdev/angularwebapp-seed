/**
 * Created by levym on 01/05/2014.
 */
"use strict";

module.exports = function(config) {
    config.set({
        basePath: "../src/",
        files: ["bower_components/angular/angular.js", "bower_components/angular-cookies/angular-cookies.js", "bower_components/angular-mocks/angular-mocks.js", "bower_components/angular-resource/angular-resource.js", "bower_components/angular-ui-router/release/angular-ui-router.js", "home/**/*.js", "test/unit/**/*.spec.js"],
        exclude: ["home/**/*.min.js"],
        preprocessors: {
            "home/**/!(*.test).js": ["coverage"]
        },
        reporters: ["progress", "coverage"],
        loggers: [
            {
                type: "console"
            }
        ],
        coverageReporter: {
            type: "html",
            dir: "../coverage/karma/unit/"
        },
        colors: true,
        logLevel: config.LOG_WARN,
        autoWatch: true,
        frameworks: ["jasmine"],
        browsers: ["Chrome"],
        captureTimeout: 60000,
        plugins: ["karma-junit-reporter", "karma-chrome-launcher", "karma-firefox-launcher", "karma-jasmine", "karma-coverage"],
        junitReporter: {
            outputFile: "test_out/unit.xml",
            suite: "unit"
        },
        singleRun: false
    });
};