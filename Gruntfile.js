/**
 * Created by levym on 01/05/2014.
 */
"use strict";

module.exports = function (grunt) {
    require("load-grunt-tasks")(grunt);
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        watch: {
            "karma-unit": {
                files: ["src/test/unit/**/*.js", "src/home/**/*.js"],
                tasks: ["karma:unit"]
            },
            "sass-dev": {
                files: ["src/sass/*.scss"],
                tasks: ["sass-dev"]
            }
        },
        env: {
            dev: {
                NODE_ENV: "development"
            },
            test: {
                NODE_ENV: "test"
            },
            prod: {
                NODE_ENV: "production"
            }
        },
        clean: {
            css: ["src/stylesheets"],
            build: ["build"],
            postbuild: ["build/stylesheets/*.css", "!build/stylesheets/*.min.css"]
        },
        sass: {
            dist: {
                files: {
                    "src/stylesheets/home.css": "src/sass/home.scss",
                    "src/stylesheets/home-old-ie.css": "src/sass/home-old-ie.scss"
                }
            },
            dev: {
                files: {
                    "src/stylesheets/home.css": "src/sass/home.scss",
                    "src/stylesheets/home-old-ie.css": "src/sass/home-old-ie.scss"
                }
            }
        },
        concat: {
            options: {
                process: false
            },
            dist: {
                src: ["src/bower_components/angular/angular.min.js", "src/bower_components/angular-resource/angular-resource.min.js", "src/bower_components/angular-cookies/angular-cookies.min.js", "src/bower_components/angular-ui-router/release/angular-ui-router.min.js", ],
                dest: "build/bower_components/combined.min.js"
            }
        },
        uglify: {
            options: {
                compress: true,
                sourceMap: true
            },
            home: {
                files: {
                    "build/home/home.min.js": ["src/home/**/*.js", "!src/home/home.min.js"]
                }
            }
        },
        processhtml: {
            homedist: {
                options: {
                    process: true
                },
                files: {
                    "build/index.html": ["src/index.html"]
                }
            }
        },
        copy: {
            html: {
                files: [
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["*.html"],
                        dest: "build/"
                    }
                ]
            },
            css: {
                files: [
                    {
                        expand: true,
                        cwd: "src/stylesheets/",
                        src: ["**"],
                        dest: "build/stylesheets/"
                    }
                ]
            },
            images: {
                files: [
                    {
                        expand: true,
                        cwd: "src/img/",
                        src: ["**"],
                        dest: "build/img/"
                    }
                ]
            },
            libs: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ["src/bower_components/html5shiv/dist/html5shiv*"],
                        dest: "build/bower_components/html5shiv/dist/",
                        rename: function (dest, src) {
                            return dest + src.replace(/\.js$/, ".min.js");
                        }
                    },
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["bower_components/respond/**/*.min.js"],
                        dest: "build/"
                    }
                ]
            },
            js: {
                files: [
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["**/*.min.*", "!bower_components/**/*.min.*"],
                        dest: "build/"
                    }
                ]
            },
            partials: {
                files: [
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["**/partials/*.html"],
                        dest: "build/"
                    }
                ]
            }
        },
        karma: {
            unit: {
                configFile: "config/karma-unit.conf.js",
                singleRun: true
            },
            e2e: {
                configFile: "config/karma-e2e.conf.js",
                singleRun: true
            }
        },
        jshint: {
            options: {
                reporter: require("jshint-stylish"),
                ignores: ["src/bower_components/**"],
                jshintrc: true
            },
            all: ["src/**/*.js"]
        },
        uncss: {
            homedist: {
                options: {
                    stylesheets: ["stylesheets/home.css"]
                },
                files: {
                    "build/stylesheets/home-tidy.css": ["src/index.html", "src/home/partials/*.html"]
                }
            },
            homeiedist: {
                options: {
                    stylesheets: ["stylesheets/home-old-ie.css"]
                },
                files: {
                    "build/stylesheets/home-old-ie-tidy.css": ["src/index.html", "src/home/partials/*.html"]
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    "build/stylesheets/home.min.css": ["build/stylesheets/home-tidy.css"]
                }
            },
            combineie: {
                files: {
                    "build/stylesheets/home-old-ie.min.css": ["build/stylesheets/home-old-ie-tidy.css"]
                }
            }
        }
    });
    grunt.registerTask("karma-unit", ["env:test", "karma:unit"]);
    grunt.registerTask("karma-e2e", ["env:test", "karma:e2e"]);
    grunt.registerTask("karma-full", ["env:test", "karma:unit", "karma:e2e"]);
    grunt.registerTask("watch-karma-unit", ["watch:karma-unit"]);
    grunt.registerTask("watch-sass-dev", ["watch:sass-dev"]);
    grunt.registerTask("sass-dist", ["clean:css", "sass:dist"]);
    grunt.registerTask("sass-dev", ["clean:css", "sass:dev"]);
    grunt.registerTask("build-dev", ["jshint", "sass-dev"]);
    grunt.registerTask("build-prod", ["jshint", "clean:build", "sass-dist", "uncss", "uglify", "concat", "copy", "processhtml", "cssmin", "clean:postbuild"]);
    grunt.registerTask("default", ["build-dev", "karma-unit"]);
};