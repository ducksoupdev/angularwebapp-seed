# Blockbuster Webapp

## Installation

### Grunt and Bower
Grunt is the build tool and Bower is the package manager. Both must be installed first:

    npm install -g grunt-cli
    npm install -g bower

If installing on Windows under Visual Studio 2012, use the switch --msvs_version=2012 when installing node modules.

### Node modules

    npm install

### Bower packages

    cd src
    bower install

## Grunt tasks

Grunt tasks for Karma tests, jshint, CSS minification, JS minification have all been included. The following tasks can be run:

Default task for creating stylesheets and running Karma unit tests
    grunt

Development build task
    grunt build-dev

Production build task
    grunt build-prod

SASS compile
    grunt sass-dev
    grunt sass-dist

Karma tests
    grunt karma-unit
    grunt karma-e2e
    grunt karma-full

Watch tasks
    grunt watch-karma-unit
    grunt watch-sass-dev
