/**
 * Created by levym on 01/05/2014.
 */
"use strict";

describe("arraySearch", function () {
    var arraySearch, testArray;

    beforeEach(module("homeApp"));

    arraySearch = null;

    beforeEach(inject(function ($injector) {
        arraySearch = $injector.get("arraySearch");
    }));

    testArray = null;

    beforeEach(function () {
        testArray = [
            {
                Id: 1,
                name: "Cheese"
            },
            {
                Id: 2,
                name: "Ham"
            },
            {
                Id: 3,
                name: "Onions"
            },
            {
                Id: 4,
                name: "Ham"
            }
        ];
    });

    describe("when searching for a single match", function () {
        it("Should return null if the array to search is null", function () {
            var item;
            item = arraySearch.single(null, function (item) {
                return item.name === "Ham";
            });
            expect(item).toBeNull();
        });
        it("returns the first item that matches", function () {
            var item;
            item = arraySearch.single(testArray, function (item) {
                return item.name === "Ham";
            });
            expect(item).toBe(testArray[1]);
        });
        it("returns null if no match found", function () {
            var item;
            item = arraySearch.single(testArray, function (item) {
                return item.name === "Hample";
            });
            expect(item).toBe(null);
        });
    });

    describe("when searching for all matches", function () {
        it("Should return null if the array to search is null", function () {
            var item;
            item = arraySearch.all(null, function (item) {
                return item.name === "Ham";
            });
            expect(item).toBeNull();
        });
        it("returns all matching elements", function () {
            var item;
            item = arraySearch.all(testArray, function (item) {
                return item.name === "Ham";
            });
            expect(item.length).toBe(2);
            expect(item[0]).toBe(testArray[1]);
            expect(item[1]).toBe(testArray[3]);
        });
        it("returns an empty array if there are no matches", function () {
            var item;
            item = arraySearch.all(testArray, function (item) {
                return item.name === "Hample";
            });
            expect(item instanceof Array).toBeTruthy();
            expect(item.length).toBe(0);
        });
    });
});