/**
 * Created by levym on 01/05/2014.
 */
"use strict";

describe("HomeController", function() {
    var homeController, mockCarsService, scope;
    homeController = null;
    scope = null;
    mockCarsService = jasmine.createSpyObj("carsService", ["getCars"]);
    mockCarsService.getCars.andCallFake(function(callback) {
        callback([
            {
                "carId": "51F270A9-6DDF-5B84-71B9-DB06C884EC56",
                "reg": "X3C5Z6",
                "make": "Nibh Aliquam Foundation",
                "colour": "green",
                "firstName": "David",
                "surname": "Buckley",
                "mobile": "0971 353 9105",
                "extension": 9,
                "location": "Gravilias"
            },
            {
                "carId": "73EB7E0E-FA46-BB05-00C1-34546E0CB170",
                "reg": "B0D1O9",
                "make": "Faucibus Ut Consulting",
                "colour": "orange",
                "firstName": "Fiona",
                "surname": "Peterson",
                "mobile": "0800 502766",
                "extension": 1,
                "location": "Eisenhï¿½ttenstadt"
            },
            {
                "carId": "790867AE-8BAB-9BA7-1699-415D97A436A1",
                "reg": "W0H6A1",
                "make": "Odio Vel Corporation",
                "colour": "red",
                "firstName": "Boris",
                "surname": "Nash",
                "mobile": "070 3379 4286",
                "extension": 2,
                "location": "Bernburg"
            }
        ]);
    });
    beforeEach(module("homeApp"));
    beforeEach(module(function($provide) {
        $provide.value("carsService", mockCarsService);
    }));
    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        homeController = $controller("HomeController", {
            $scope: scope
        });
    }));
    it("Should load the cars for the setting", function() {
        expect(scope.cars.length).toBe(3);
    });
});