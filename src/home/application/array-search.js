angular.module("homeApp")
    .factory("arraySearch", [function () {
        "use strict";
        return {
            single: function (array, compareFunc) {
                if (!array) {
                    return null;
                }
                for ( var index = 0; index < array.length; index++) {
                    var item = array[index];
                    if ( compareFunc(item) ) {
                        return item;
                    }
                }
                return null;
            },

            all: function (array, compareFunc) {
                if (!array) {
                    return null;
                }
                var matches = [];
                for ( var index = 0; index < array.length; index++) {
                    var item = array[index];
                    if ( compareFunc(item) ) {
                        matches.push(item);
                    }
                }
                return matches;
            }
        };
    }]);