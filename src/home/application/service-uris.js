// Needs to be a 'factory' service rather than a 'value' service because it relies on window.global being initialised
// and it seems that angular doesn't wait for boot-strapping for 'value' services.
angular.module("homeApp")
    .factory("globalServiceUris", function() {
        "use strict";
        return {
            carsUri: window.QUERY_HOST + "/api/cars/:reg"
        };
    });